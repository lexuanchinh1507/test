package com.example.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping
    public String hello() {
        return checkMethodName("check ", "method name");
    }

    public String checkMethodName(String a, String b) {
        return a + b;
    }
}
