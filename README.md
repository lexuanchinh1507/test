**How to install gitlab runner on macos**

1. Install gitlab runner 

- brew install gitlab-runner
- brew services start gitlab-runner

Reference: https://docs.gitlab.com/runner/install/osx.html

2. Register gitlab runner
- gitlab-runner register

Please get url and token from this link https://gitlab.com/lexuanchinh1507/test/-/settings/ci_cd

Enter the GitLab instance URL (for example, https://gitlab.com/)

Enter the registration token: Y_42MyysQ6BKHhc-EQri

Enter an executor: docker, docker-ssh, shell, ssh, virtualbox, docker-ssh+machine, kubernetes, custom, parallels, docker+machine: docker

Enter the default Docker image (for example, ruby:2.6): ruby:2.6

Reference: https://docs.gitlab.com/runner/register/index.html


-------- ** ------
**How to install gitlab runner using docker**

**Note: On macOS, use /Users/Shared instead of /srv**

- install gitlab-runner using docker
   docker run -d --name gitlab-runner --restart always \
     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     gitlab/gitlab-runner:latest
- docker run --rm -it -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register

Please get url and token from this link https://gitlab.com/lexuanchinh1507/test/-/settings/ci_cd

Enter the GitLab instance URL (for example, https://gitlab.com/)

Enter the registration token: Y_42MyysQ6BKHhc-EQri

Enter an executor: docker, docker-ssh, shell, ssh, virtualbox, docker-ssh+machine, kubernetes, custom, parallels, docker+machine: docker

Enter the default Docker image (for example, ruby:2.6): ruby:2.6

Reference: https://docs.gitlab.com/runner/install/docker.html


**Gitlab Runner**

1. GitLab Runner
- Gitlab Runner is an application that works with Gitlab CI/CD to run jobs in a pipeline.
- You can install Gitlab-Runner on operating systems such as macOS, Linux, Windows. It is also supported to install on docker environment.
- There are three types of runners, including Shared runners, Specific runners, Group runners.
  + Shared runners are for use by all projects
  + Group runners are for all projects and subgroups in a group
  + Specific runners are for individual projects
2. Runner registration
- After you install Gitlab-Runner, you register individual runners to run Specific Runners.
- If you want to use only Specific Runners to run your job, please disable Shared runners.
3. Executors
- When you register a runner, you must choose an executor.
- There are several types of executors such as SSH, Shell, VirtualBox Parallels, Docker, Kubernetes, Custom. Currently, we are using docker executor. 
